# Wardrobify

Team:

* Person 1 - Tiffany Ameral - Shoes 
* Person 2 - Justin Thomas - Hats

## Design

![planning diagram](planning_diagram.png)

## Shoes microservice

<!-- Explain your models and integration with the wardrobe
microservice, here. -->

Models:
Shoes:
Name
Manufacturer 
Color
pictureURL
bin

Itegrates with wardrobe via a several avenues: 
*views.py
*the admin pannel 
*Shoes.js & NewSHoe.js

Bins:
*closet_name
*bin_number
*bin_size

## Hats microservice

Models:
Hat
Attributes:
- fabric
- style name
- color
- picture_url
- VO location (foreign key)

VOLocation
- closet_name
- section_number
- shelf_number


