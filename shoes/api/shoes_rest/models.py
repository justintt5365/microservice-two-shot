from django.db import models

# Create your models here.

class BinsVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)

class Shoes(models.Model):
    name = models.CharField(max_length=200)
    manufacturer = models.CharField(max_length=200)
    color = models.CharField(max_length=50)
    picture_url = models.URLField(null = True)
    bin = models.ForeignKey(BinsVO, related_name="shoes", on_delete=models.CASCADE,null=True)

    def __str__(self):
        return self.name

